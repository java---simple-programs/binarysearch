package com.company;
import java.util.Scanner;

public class Main {

    public boolean binarySearch(int[] numbers, int number){
        int indexLow=0;
        int indexHigh=numbers.length-1;
        while(indexLow<=indexHigh){
            int indexMiddle=indexLow+(indexHigh-indexLow)/2;

            if(number<numbers[indexMiddle]){
                indexHigh=indexMiddle-1;
            }
            else if(number>numbers[indexMiddle]){
                indexLow=indexMiddle+1;
            }
            else{
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner S=new Scanner(System.in);
        System.out.println("how long is your tab? ");
        int longTab=S.nextInt();
        int[] numbers=new int[longTab];

        for(int i=0;i<numbers.length;i++){
            System.out.print("Please enter the number");
            numbers[i]=S.nextInt();
        }

        for(int i=0;i<numbers.length;i++){
            System.out.print(numbers[i]);
        }


    }
}
